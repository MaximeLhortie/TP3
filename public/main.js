$(document).ready(function() {
	$("#fLogin").on("submit", function(event) {
		event.preventDefault();
		var username = $("#login").val();
		var password = $("#pwd").val();
		if (username !== "" && password !== "") {
			var request = $.ajax({
				url: "/login",
				method: "POST",
				dataType : "json",
				data : {username : username, password : password}
			});
			request.done(function(result) {
				console.log("done");
				if(result.status){
					alert('Connexion réussie. Un cookie a été crée et déposé');
				}
				else{
					alert('Identifiants incorrects.');
				}

			});
			request.fail(function(jqXHR, textStatus) {
				alert('Connexion au serveur impossible. Erreur : ' + textStatus);
			});
		}
		else{
			alert('Vous devez entrer un login et un mot de passe');
		}
	});
});